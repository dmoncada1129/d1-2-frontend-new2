/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.deutsche.dba.web;


import com.deutsche.dba.utils.SimpleJsonMessage;
import deutschebank.core.ApplicationScopeHelper;
import deutschebank.core.UserController;
import deutschebank.dbutils.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;


/**
 *
 * @author Selvyn
 */
@Path("/services")
public class DBDAServicePort implements IDBDAServicePort
{
    final   UserController userController = new UserController();

    @Override
    @GET
    @Path("/sayhello")
    public Response sayHtmlHelloTest()
    {
        String result = "<html> " + "<title>" + "DBDA" + "</title>"
                + "<body><h1>" + "the dbda is running..." + "</h1></body>" + "</html> ";

        return Response.status(200).entity(result).build();
    }
    
    @Override
    @GET
    @Path("/getAllTags")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAllTags()
    {
        String result = "getAllTags() need to be built";
    	return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
    }
    
    @Override
    @GET
    @Path("/getAllURL")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getAllURL()
    {
        String result = "getAllURL() need to be built";
    	return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
    }

    @Override
    @GET
    @Path("/get/{tags}")
    public Response getSavedURLWithInfo(@PathParam("tags")String tags)
    {
        String result = "getSavedURLWithInfo() need to be built";
    	return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
    }

    @Override
    @GET
    @Path("/login/{usr}/{pwd}")
    public Response loginWithInfo( @PathParam("url")String usr,
                                        @PathParam("description")String description,
                                        @PathParam("tags")String tags )
    {
        String result = "loginWithInfo() need to be built";
    	return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
    }

    @Override
    @POST
    @Path("/login")
    public Response loginWithInfoFromForm( @FormParam("usr")String usr,
                                        @FormParam("pwd")String pwd )
    {
        String result = userController.verifyLoginDetails(usr, pwd);

        if( result != null)
        {
            return Response.ok(result, MediaType.APPLICATION_JSON_TYPE).build();
        }
        else
            return Response.status(400).entity(new SimpleJsonMessage("User could not be found")).build();
    }

    @GET
    @Path("/report/deal")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getDealReport()
    {
        // call function to extract data from database
        DealController dealController = new DealController();
        String deals = dealController.getDeals();
        return Response.ok(deals, MediaType.APPLICATION_JSON_TYPE).build();
//        try {
//            java.net.URI location = new java.net.URI("/data-analyzer-webfront/src/main/webapp/dealReport.jsp");
//            return Response.temporaryRedirect(location).build();
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//            return null;
//        }
    }

    @GET
    @Path("/report/counterparty")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getCounterpartyReport()
    {
        // call function to extract data from database
        CounterpartyController counterpartyController = new CounterpartyController();
        String counterparties = counterpartyController.getCounterparties();
//        System.out.println("*********************"+counterparties);
        return Response.ok(counterparties, MediaType.APPLICATION_JSON_TYPE).build();
    }

    @GET
    @Path("/report/instrument")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getInstrumentReport()
    {
        // call function to extract data from database
        InstrumentController instrumentController = new InstrumentController();
        String instruments = instrumentController.getInstruments();
//        System.out.println("*********************"+instruments);
        return Response.ok(instruments, MediaType.APPLICATION_JSON_TYPE).build();
    }

}
