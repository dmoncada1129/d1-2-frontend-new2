var rootURL = "rws/services";
var counterpartyData;

$(function () {
    var request = $.ajax({
        method: 'GET',
        url: rootURL + '/report/counterparty',
        dataType: "json", // data type of response

        success: function ( result) {
            dealData =result;
        }
    });
    request.fail(function( jqXHR, textStatus, errorThrown ) {
        alert( "Request " + textStatus + ", fail to retrieve counterparty data"  );
    });
});


$(function () {
    $('#table').bootstrapTable({
        data: counterpartyData
    });
});
